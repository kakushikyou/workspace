# 开发中遇到的必备英语语句和单词

### 列表

1. experience ：your experience

2. tailor ： tailor your experience

3. describe（describes）：What describes your experience with source control ？

   I'm an advanced Git user.(资深，高级)

   I know enough Git to get things done。

   I‘ve used source control tools。

   I have no experience。

4. What best describe your role ？

   Developer

   Manager

   Designer

   Student

   Other

5. What best describes how you plan to use Bitbucket ？

   Design Review

   Learning Git

   Application Development

   Mobile Development

   Web Development

   Development（other）

   Something else

6. Confirm access to your account

   **Sourcetree for Windows** is requesting access to the following:

   - Read and modify your account information
   - Read and modify your repositories' issues
   - Read and modify your workspace's project settings, and read and transfer repositories within your workspace's projects
   - Read and modify your repositories and their pull requests
   - Administer your repositories
   - Read and modify your snippets
   - Read and modify your team membership information
   - Read and modify your repositories' webhooks
   - Read and modify your repositories' wikis

   By installing the App you agree to the [privacy policy](https://www.atlassian.com/legal/privacy-policy) and [terms of use](https://www.atlassian.com/legal/cloud-terms-of-service) provided by Sourcetree for Windows.

7. is Private