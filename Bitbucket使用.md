鉴于目前中文各类博客喜欢东抄西抄，摆渡的糟糕推荐等情况，本文博客参考官网文档，详细介绍了Bitbucket的个人账号注册、Team团队创建、团队仓库创建、仓库/项目（Repository/Project）的区别。欢迎交流讨论。
bitbucket使用初体验
选择Bitbucket的理由
个人账号注册
Team团队创建
Step1-团队创建
Step2-团队增加成员
仓库项目创建
仓库（Repository）、项目（Project）区别
Step1
私有账户Double Check
Repository网页访问
Team网页访问
官网参考链接
选择Bitbucket的理由
核心原因是Bitbucket可以创建更多更大的私有仓库，对于研究、小组内分享该功能非常重要。

个人账号注册
个人账号注册即按照流程，输入注册邮箱、设置密码即可。需要注意的是两个名字的设置：

Account settings name：账户名称，会显示在个人主页左边栏等处。
Bitbucket profile settings UserName：用户名称，默认UserName是个人主页的域名。
账户名称 不可更改，可重名；
用户名称 可更改，不可重名。

Team团队创建
官方文档参考：create and administer your team.

Step1-团队创建
team name：团队名称
Workspace ID：团队域名，团队的仓库、code等都在此域名下
建立后上述信息可以修改。


Step2-团队增加成员


点击增加成员，输入邮箱，设置分类群组，即向该用户发送了请求，通过后即成功加入。
bitbucket的默认群组有2个group: Administrator, Developer
二者的权限范围通过点击Administrator, Developer可浏览：


bitbucket还有其他权限可以设置，详情可参考官方文档：organize your team into user groups

仓库项目创建
仓库（Repository）、项目（Project）区别
Repository：存放代码的仓库。
Project：项目管理工具（看板、项目进度管理、issue 解决进度等，类似于 jira）。

当UserName为团队TeamName成功创建一个Repository之后，仓库显示界面关系为：

TeamName/ProjectName
RepositoryName
https:// UserName@bitbucket.org / TeamName / RepositoryName.git
1
2
3
我们可以创建多个Repository放于同一个Project下。

Step1
如果为私有仓库，点击Private repository即可。

之后即成功创建一个Project下的仓库。

私有账户Double Check
登陆不在群组内的账户，测试是否可以看到Repository项目。

Repository网页访问
当直接访问Repository网页时候，私有仓库会有如下显示：



Team网页访问

仅限Administrator可见。
